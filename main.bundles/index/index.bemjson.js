module.exports = {
  block: 'page',
  title: '{{foxexpo-title}}',
  head: [{
    elem: 'css',
    url: 'index.min.css'
  }, {
    elem: 'meta',
    attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1.0' }
  }],
  scripts: [
    /*{
            elem: 'js',
            url: 'https://yastatic.net/jquery/2.2.0/jquery.min.js'
          }, */
    {
      elem: 'js',
      url: 'index.min.js'
    }, {
      elem: 'conditional-comment',
      condition: 'lte IE 9',
      content: {
        elem: 'js',
        url: '/frontend/js/modules/html5shiv.js'
      }
    }
  ],
  mix: [{
    block: 'b-page',
    mods: { fixed: 'top' },
    js: true
  }],
  content: [{
    block: 'b-header',
    js: true,
    tag: 'header',
    mods: {
      fixed: true
    },
    content: [{
      block: 'b-nav',
      content: [{
        elem: 'i-group',
        elemMods: {
          left: true
        },
        content: [{
          elem: 'item',
          content: [{
            block: 'b-logo',
            content: [{
              elem: 'i',
              elemMods: {
                img: true
              },
              content: [{
                elem: 'link',
                tag: 'a',
                attrs: {
                  href: '#'
                },
                content: {
                  mix: [{
                    block: 'b-logo',
                    elem: 'img',
                    // elemMods: {'responsive': true}
                  }],
                  block: 'image',
                  content: {
                    tag: 'svg',
                    cls: 'b-logo__img_hidden_xs',
                    attrs: {
                      xmlns: 'http://www.w3.org/2000/svg',
                      viewBox: '0 0 283 283',
                      width: '100px',
                      height: '100px'
                    },
                    content: '<path fill-rule="evenodd" clip-rule="evenodd" fill="#EC8660" d="M187.3 233.8v-91.2l82.1-42.1v84.1l-82.1 49.2zM98.8-.5l80 50.9L99 91.7 98.8-.5zm170.6.3l-80 50.9L269.2 92l.2-92.2zM93 196.1l-78.9-39.9v83.4L93 283v-86.9zm91.1-58.8l79.2-40.9-79.2-40.6L106 96.4l78.1 40.9zM93 100.5V190l-78.9-40.1L93 100.5zm5.9 89.5v-89.5l82.5 42.1v91.2L98.9 190zm30.6-164L106 12v26.8L129.5 26zm109.2.3l23.4-14V39l-23.4-12.7zM87.1 271.9l-24.3-13.1 24.3-13.3v26.4z"/>'
                  },
                  attrs: {
                    alt: 'FoxExpo'
                  }
                }
              }]
            }, {
              elem: 'i',
              elemMods: {
                text: true
              },
              content: [{
                elem: 'link',
                tag: 'a',
                attrs: {
                  href: '#'
                },
                content: [{
                  elem: 'text',
                  tag: 'span',
                  content: 'FOX'
                }, {
                  elem: 'text',
                  tag: 'span',
                  elemMods: {
                    light: true
                  },
                  content: 'EXPO',
                }]
              }]
            }]
          }]
        }]
      }, {
        elem: 'i-group',
        elemMods: {
          right: true
        },
        content: [{
          elem: 'item',
          content: [{
            block: 'b-phone',
            content: [{
              block: 'image',
              alt: 'phone Icon',
              mix: [{
                block: 'b-phone',
                elem: 'icon'
              }],
              content: {
                tag: 'svg',
                cls: 'b-phone__img_responsive',
                attrs: {
                  xmlns: 'http://www.w3.org/2000/svg',
                  viewBox: '0 0 200 200',
                  width: '100px',
                  height: '100px'
                },
                content: '<path d="M107.613 128.296l11.538-6.027c4.335-2.163 4.116-2.54 6.648 2.853l9.893 21.063c1.21 2.57 2.263 4.763-2.718 6.99-27.783 12.42-48.095-14.24-48.095-14.24s-12.83-11.915-22.91-50.865c-7.204-27.838 8.55-40.425 18.092-45.396 6.397-3.332 7.46-1.385 9.992 4.035 1.108 2.372 6.817 14.662 9.906 21.316 1.04 2.237.51 4.61-1.645 5.646-3.536 1.7-9.574 4.53-12.99 6.086-1.752.796-3.32 2.6-2.02 5.372l19.364 41.27c.79 1.85 1.73 3.576 4.946 1.896z" fill="#ec8660" fill-rule="evenodd" stroke="#ec8660" stroke-width=".208"/>'
              }
            }, {
              elem: 'i-group',
              content: [{
                elem: 'item',
                content: {
                  block: 'link',
                  mix: [{ block: 'b-phone', elem: 'link' }],
                  url: 'tel:+375259194771',
                  content: '+375 25 919 47 71'
                }
              }, {
                elem: 'item',
                content: {
                  block: 'link',
                  mix: [{ block: 'b-phone', elem: 'link' }],
                  url: 'tel:+375259194771',
                  content: '+375 33 669 47 71'
                }
              }]
            }]
          }]
        }]
      }, {
        elem: 'i-group',
        elemMods: {
          center: true
        },
        content: [{
          elem: 'item',
          content: [{
            block: 'b-menu',
            js: true,
            content: [{
              block: 'button',
              mix: [{ block: 'b-menu', elem: 'controls' }],
              mods: { theme: 'islands', size: 'xl', view: 'plain', togglable: 'check' },
              icon: {
                block: 'icon',
                content: '<svg xmlns="http://www.w3.org/2000/svg" width="50" viewBox="0 0 50 50" height="50" fill="#fff"><path d="M0 0h50v50H0z" fill="none"/><path d="M6.25 37.5h37.5v-4.167H6.25V37.5zm0-10.417h37.5v-4.166H6.25v4.166zm0-14.583v4.167h37.5V12.5H6.25z"/></svg>'
              }
            }, {
              block: 'menu',
              js: true,
              tag: 'ul',
              mix: [{
                block: 'b-menu',
                elem: 'content',
                elemMods: {
                  status: 'closed'
                }
              }],
              content: [{
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/index/index.html',
                  content: 'Главная'
                }]
              }, {
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/specials/specials.html',
                  content: 'Акции'
                }]
              }, {
                block: 'menu-item',
                tag: 'li',
                mods: { disabled: true },
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }, {
                  block: 'b-menu',
                  elem: 'item',
                  elemMods: {
                    submenu: true
                  }
                }],
                content: [{
                  block: 'b-submenu',
                  js: true,
                  content: [{
                    block: 'button',
                    mix: [{ block: 'b-submenu', elem: 'controls' }],
                    mods: { 'menu-switcher': true, type: 'link', theme: 'islands', size: 'xl', view: 'plain', togglable: 'check' },
                    icon: {
                      block: 'icon',
                      mix: [{ block: 'visible', mods: { sm: true } }],
                      content: '<svg xmlns="http://www.w3.org/2000/svg" width="24" viewBox="0 0 24 24" height="24" fill="#ec8660"><path d="M7.41 8.295l4.59 4.58 4.59-4.58L18 9.705l-6 6-6-6z"/><path fill="none" d="M0-.75h24v24H0z"/><path d="M12 0C5.384 0 0 5.384 0 12s5.384 12 12 12 12-5.384 12-12S18.616 0 12 0zm0 1.924c5.576 0 10.076 4.5 10.076 10.076 0 5.576-4.5 10.076-10.076 10.076-5.576 0-10.076-4.5-10.076-10.076 0-5.576 4.5-10.076 10.076-10.076z" style="line-height:normal;text-indent:0;text-align:start;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000000;text-transform:none;block-progression:tb;isolation:auto;mix-blend-mode:normal" color="#000" font-family="sans-serif" white-space="normal" overflow="visible" solid-color="#000000"/></svg>'
                    },
                    url: '#',
                    text: 'Печать'
                  }, {
                    block: 'menu',
                    js: true,
                    mods: { focused: false },
                    mix: [{ block: 'b-submenu', elem: 'content', elemMods: { status: 'closed' } }],
                    tag: 'ul',
                    content: [{
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Визитки'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Флаеры'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Буклеты'
                      }
                    }, {
                      block: 'menu-item',
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Пакеты'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Майки'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Ручки'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Кружки'
                      }
                    }, {
                      block: 'menu-item',
                      mods: { type: 'link' },
                      mix: [{ block: 'b-submenu', elem: 'item' }],
                      tag: 'li',
                      content: {
                        block: 'link',
                        mix: { block: 'b-submenu', elem: 'link' },
                        url: '#',
                        content: 'Широкоформатная печать'
                      }
                    }]
                  }]
                }]
              }, {
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/catalog/catalog.html',
                  content: 'Стенды'
                }]
              }, {
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/catalog/catalog.html',
                  content: 'Аренда'
                }]
              }, {
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/catalog/catalog.html',
                  content: 'Дизайн'
                }]
              }, {
                // Печать
                block: 'menu-item',
                tag: 'li',
                mix: [{
                  block: 'b-menu',
                  elem: 'item'
                }],
                content: [{
                  block: 'link',
                  mix: [{ block: 'b-menu', elem: 'link' }],
                  url: '/main.bundles/about-us/about-us.html',
                  content: 'Контакты'
                }]
              }]
            }]
          }]
        }]
      }]
    }]
  }, {
    block: 'b-main-panel',
    mix: [{
      block: 'clearfix'
    }],
    content: [{
      elem: 'i-left',
      content: {
        block: 'b-slider',
        js: true,
        mix: { block: 'hidden', mods: { sm: true } },
        slidelist: [{
          thumb: {
            url: '../../main.blocks/frontend/img/slideImage.jpg',
            alt: 'slideImage-0'
          },
          image: {
            url: '../../main.blocks/frontend/img/slideImage.jpg',
            alt: 'slideImage-0'
          },
          title: '<p>Оцените по достоинству!</p><p>Pop up Display Stand</p>',
          properties: [{
            icon: 'arrow',
            desc: '336x231 см'
          }, {
            icon: 'auto',
            desc: 'Бесплатная доставка'
          }, {
            icon: 'clock',
            desc: '15 мин'
          }, {
            icon: 'brush',
            desc: 'Макет в подарок'
          }],
          footer: 'Всего за 910$'
        }, {
          thumb: {
            url: '../../main.blocks/frontend/img/slideImage.jpg',
            alt: 'slideImage-1'
          },
          image: {
            url: '../../main.blocks/frontend/img/slideImage2.jpg',
            alt: 'slideImage-1'
          },
          title: '<p>Оцените по достоинству!</p><p>Pop up Display Stand</p>',
          properties: [{
            icon: 'arrow',
            desc: '336x231 см'
          }, {
            icon: 'auto',
            desc: 'Бесплатная доставка'
          }, {
            icon: 'clock',
            desc: '15 мин'
          }, {
            icon: 'brush',
            desc: 'Макет в подарок'
          }],
          footer: 'Всего за 920$'
        }, {
          thumb: {
            url: '../../main.blocks/frontend/img/slideImage.jpg',
            alt: 'slideImage-2'
          },
          image: {
            url: '../../main.blocks/frontend/img/slideImage3.jpg',
            alt: 'slideImage-2'
          },
          title: '<p>Оцените по достоинству!</p><p>Pop up Display Stand</p>',
          properties: [{
            icon: 'arrow',
            desc: '336x231 см'
          }, {
            icon: 'auto',
            desc: 'Бесплатная доставка'
          }, {
            icon: 'clock',
            desc: '15 мин'
          }, {
            icon: 'brush',
            desc: 'Макет в подарок'
          }],
          footer: 'Всего за 930$'
        }, {
          thumb: {
            url: '../../main.blocks/frontend/img/slideImage.jpg',
            alt: 'slideImage-3'
          },
          image: {
            url: '../../main.blocks/frontend/img/slideImage4.jpg',
            alt: 'slideImage-3'
          },
          title: '<p>Оцените по достоинству!</p><p>Pop up Display Stand</p>',
          properties: [{
            icon: 'arrow',
            desc: '336x231 см'
          }, {
            icon: 'auto',
            desc: 'Бесплатная доставка'
          }, {
            icon: 'clock',
            desc: '15 мин'
          }, {
            icon: 'brush',
            desc: 'Макет в подарок'
          }],
          footer: 'Всего за 940$'
        }]
      }
    }]
  }, {
    block: 'b-cta-form',
    js: true,
    attrs: { action: '', method: 'post', enctype: 'multipart/form-data' },
    tag: 'form',
    mods: {
      horiz: true
    },
    content: [{
      elem: 'caption',
      content: [
        '<p>Хотите получить информацию быстрее?</p><p>Укажите ваше имя и телефон, и мы сами перезвоним Вам!</p>'
      ]
    }, {
      block: 'input',
      name: 'bctaform_check',
      mods: { hidden: true },
      val: '46b3931b9959c927df4fc65fdee94b07y'
    }, {
      elem: 'input-group',
      mix: [{
        block: 'control-group'
      }],
      elemMods: {
        left: true,
      },
      content: [{
        block: 'input',
        name: 'bctaform_name',
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            text: true
          }
        }],
        val: 'Ваше Имя',
        mods: {
          focused: false,
          theme: 'islands',
          'has-clear': true,
          'has-required': true,
          valid: true,
          size: 'l'
        },
        placeholder: 'Ваше имя'
      }, {
        block: 'input',
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            text: true
          }
        }],
        mods: {
          focused: false,
          'has-clear': true,
          theme: 'islands',
          size: 'l'
        },
        placeholder: 'Телефон'
      }, {
        block: 'button',
        name: 'bctaform_submit',
        mix: [{
          block: 'b-cta-form',
          elem: 'button',
          elemMods: {
            submit: true
          }
        }],
        mods: {
          focused: false,
          theme: 'islands',
          size: 'xl'
        },
        text: 'Перезвоните мне!',
        icon: {
          block: 'spin',
          mods: { theme: 'islands', size: 's' }
        }
      }]
    }, {
      block: 'link',
      mods: { pseudo: true },
      content: {
        block: 'popup',
        js: { popupDefaultErrMsg: 'Не заполнены обязательные поля' },
        mods: { autoclosable: true, theme: 'islands', target: 'anchor' },
        directions: ['top-left', 'top-right'],
        content: 'Не заполнены обязательные поля'
      }
    }]
  }, {
    block: 'b-content',
    content: [{
      block: 'b-adv',
      mix: [{
        block: 'clearfix'
      }],
      content: [{
          elem: 'i-group',
          elemMods: {
            center: true
          },
          content: [{
            elem: 'caption',
            elemMods: {
              'bottom-line': true
            },
            content: 'Наши услуги'
          }, {
            block: 'b-adv-list',
            advlist: [{
              icon: "printer",
              caption: "Услуги печати",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }, {
              icon: "bbook",
              caption: "Разработка фирменного стиля",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }, {
              icon: "stand",
              caption: "Аренда стендов",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }, {
              icon: "cook",
              caption: "Оформление торжеств",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }, {
              icon: "p-design",
              caption: "Дизайн печатной продукции",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }, {
              icon: { src: "../../main.blocks/frontend/img/icon_2.png" },
              caption: "Web-дизайн",
              desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad vero nulla impedit obcaecati, nemo exercitationem vitae, iusto neque inventore tempora sapiente eaque nam ullam ipsum assumenda officia. Dicta, id, maxime."
            }]
          }]
        }
        /*, {
                elem: 'i-group',
                elemMods: {
                  left: true
                },
                content: [{
                  elem: 'img',
                  tag: 'img',
                  attrs: {
                    src: '../../main.blocks/frontend/img/adv-image.jpg',
                    alt: 'adv Image',
                    width: '100%',
                    height: 'auto'
                  }
                }]
              }*/
      ]
    }, {
      block: 'b-promo',
      content: [{
        elem: 'title',
        content: 'Акции'
      }, {
        elem: 'content',
        content: [{
          elem: 'controls',
          content: {
            block: 'select',
            mods: {
              mode: 'check',
              theme: 'islands',
              size: 'xl'
            },
            name: 'print_prod_item',
            val: [2],
            text: 'Выберите категорию',
            options: [{
              val: 1,
              text: 'Печатная продукция'
            }, {
              val: 2,
              text: 'Выставочное оборудование'
            }]
          }
        }, {
          block: 'b-promo-gallery',
          promoslides: [{
            img: "../../main.blocks/frontend/img/gallery-item-0.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }, {
            img: "../../main.blocks/frontend/img/gallery-item-1.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }, {
            img: "../../main.blocks/frontend/img/gallery-item-2.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }, {
            img: "../../main.blocks/frontend/img/gallery-item-3.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }, {
            img: "../../main.blocks/frontend/img/gallery-item-4.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }, {
            img: "../../main.blocks/frontend/img/gallery-item-5.jpg",
            price: "80$",
            desc: "Roll-up 1*2"
          }]
        }, {
          block: 'button',
          mods: {
            theme: 'islands',
            size: 'xl',
            type: 'link'
          },
          mix: [{
            block: 'b-promo',
            elem: 'button',
            elemMods: {
              next: true
            }
          }],
          text: 'Далее'
        }]
      }]
    }, {
      block: 'b-cl-line',
      caption: 'Наши клиенты',
      clients: ["../../main.blocks/frontend/img/clients-item-0.png", "../../main.blocks/frontend/img/clients-item-1.png", "../../main.blocks/frontend/img/clients-item-2.png", "../../main.blocks/frontend/img/clients-item-3.png", "../../main.blocks/frontend/img/clients-item-4.png", "../../main.blocks/frontend/img/clients-item-5.png"]
    }, {
      block: 'b-cta-form',
      js: true,
      attrs: { action: '', method: 'post', enctype: 'multipart/form-data' },
      tag: 'form',
      mods: {
        horiz: true
      },
      content: [{
        elem: 'caption',
        content: [
          '<p>Хотите получить подробную информацию о ценах?</p><p>Заполните эту форму и скорее отправьте ее нам!</p>'
        ]
      }, {
        block: 'input',
        name: 'bctaform_check',
        mods: { hidden: true },
        val: '46b3931b9959c927df4fc65fdee94b07y'
      }, {
        elem: 'input-group',
        mix: [{
          block: 'control-group'
        }],
        elemMods: {
          left: true,
        },
        content: [{
          block: 'input',
          name: 'bctaform_name',
          mix: [{
            block: 'b-cta-form',
            elem: 'input',
            elemMods: {
              text: true
            }
          }],
          val: 'Ваше Имя',
          mods: {
            focused: false,
            theme: 'islands',
            'has-clear': true,
            'has-required': true,
            valid: true,
            size: 'l'
          },
          placeholder: 'Ваше имя'
        }, {
          block: 'input',
          name: 'bctaform_email',
          mix: [{
            block: 'b-cta-form',
            elem: 'input',
            elemMods: {
              text: true
            }
          }],
          mods: {
            valid: 'email',
            focused: false,
            theme: 'islands',
            'has-clear': true,
            'has-required': true,
            size: 'l'
          },
          placeholder: 'e-mail'
        }, {
          block: 'input',
          mix: [{
            block: 'b-cta-form',
            elem: 'input',
            elemMods: {
              text: true
            }
          }],
          mods: {
            focused: false,
            'has-clear': true,
            'has-required': true,
            theme: 'islands',
            valid: true,
            size: 'l'
          },
          placeholder: 'Телефон'
        }, {
          block: 'button',
          name: 'bctaform_submit',
          mix: [{
            block: 'b-cta-form',
            elem: 'button',
            elemMods: {
              submit: true
            }
          }],
          mods: {
            focused: false,
            theme: 'islands',
            size: 'xl'
          },
          text: 'Получить подробную информацию!',
          icon: {
            block: 'spin',
            mods: { theme: 'islands', size: 's' }
          }
        }]
      }, {
        block: 'link',
        mods: { pseudo: true },
        content: {
          block: 'popup',
          js: { popupDefaultErrMsg: 'Не заполнены обязательные поля' },
          mods: { autoclosable: true, theme: 'islands', target: 'anchor'},
          directions: ['bottom-center', 'top-right'],
          content: 'Не заполнены обязательные поля'
        }
      }]
    }]
  }, {
    block: 'b-footer',
    tag: 'footer',
    content: [{
      elem: 'i-group',
      content: [{
        elem: 'i-group',
        elemMods: { 'icon': true },
        content: {
          block: 'image',
          content: '<svg fill="#ec8660" height="30" viewBox="0 0 24 24" width="30" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z" fill="none"/><path d="M20 0H4v2h16V0zM4 24h16v-2H4v2zM20 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-8 2.75c1.24 0 2.25 1.01 2.25 2.25s-1.01 2.25-2.25 2.25S9.75 10.24 9.75 9 10.76 6.75 12 6.75zM17 17H7v-1.5c0-1.67 3.33-2.5 5-2.5s5 .83 5 2.5V17z"/></svg>'
        }
      }, {
        elem: 'i',
        content: ['<p>ИП Безбердая А. Ю. УНП №192291695</p>', '<p>Адрес г. Минск ул. Жудро 26-4</p>']
      }]
    }, {
      elem: 'i-group',
      content: [{
        elem: 'i',
        content: [{
          block: 'b-contacts',
          content: [{
            block: 'link',
            url: 'mailto: foxexpo.by@gmail.com',
            mix: [{
              block: 'b-footer',
              elem: 'i',
              tag: 'span',
              elemMods: {
                mail: true
              },
            }],
            content: [{
              block: 'icon',
              content: '<svg xmlns="http://www.w3.org/2000/svg" fill="#ec8660" viewBox="0 0 20 20" height="20" width="20"><path d="M19.033 1.91c-2.054.105-4.12-.015-6.18.03-4.168.013-8.342-.026-12.507.02-.56.276-.214 1.086-.337 1.6.007 4.528-.037 9.06.02 13.588-.082.46.066.953.6.932 6.297.01 12.607 0 18.91.01.78-.382.302-1.38.43-2.05V2.376c-.11-.385-.592-.473-.938-.46zM2.14 3.72c1.727.132 3.47-.017 5.207.037 3.554.016 7.235-.033 10.71.024-.573.496-1.29.804-1.866 1.306L9.943 9.42C7.257 7.533 4.514 5.646 1.867 3.76c.084-.042.182-.04.274-.042zm16.012 12.548H1.822V6.114c2.663 1.83 5.283 3.753 7.973 5.526.87-.06 1.415-.888 2.162-1.255l6.196-4.296v10.178z"/></svg>'
            }, 'foxexpo.by@gmail.com']
          }, {
            block: 'link',
            mix: [{
              block: 'b-footer',
              elem: 'i',
              elemMods: {
                phone: true
              }
            }],
            url: 'tel:+375259194771',
            content: [{
              block: 'icon',
              content: '<svg xmlns="http://www.w3.org/2000/svg" fill="#ec8660" viewBox="0 0 20 20" height="20" width="20"><path d="M19.55 15.82l-3.087-3.097c-.615-.614-1.634-.595-2.27.043l-1.557 1.56-.307-.172c-.984-.546-2.33-1.294-3.744-2.713C7.166 10.02 6.42 8.67 5.874 7.683c-.058-.104-.113-.205-.168-.3L6.75 6.337l.514-.516c.638-.638.655-1.66.042-2.275L4.216.45c-.613-.616-1.632-.597-2.27.042l-.87.877.024.023c-.293.373-.536.804-.718 1.268-.167.443-.27.865-.32 1.287-.407 3.39 1.138 6.486 5.33 10.69 5.797 5.808 10.468 5.37 10.67 5.348.44-.053.86-.158 1.287-.324.458-.18.887-.423 1.26-.715l.018.017.882-.866c.637-.64.655-1.66.04-2.277z"/></svg>'
            }, '+375 25 919 47 71']
          }]
        }, {
          block: 'b-soc-icons',
          content: [{
            block: 'link',
            mix: [{
              block: 'b-soc-icons',
              elem: 'icon',
            }],
            url: 'www.facebook.com/foxexpo.by',
            content: [{
              block: 'icon',
              content: '<svg fill="#ec8660" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 49.999997 50"><path d="M37.5.01L31.014 0C23.73 0 19.023 4.83 19.023 12.305v5.673h-6.52c-.563 0-1.02.458-1.02 1.02v8.22c0 .564.458 1.02 1.02 1.02h6.52V48.98c0 .564.456 1.02 1.02 1.02h8.505c.563 0 1.02-.457 1.02-1.02V28.24h7.622c.563 0 1.02-.456 1.02-1.02l.002-8.22c0-.27-.107-.53-.298-.72-.19-.192-.45-.3-.722-.3h-7.625v-4.81c0-2.31.55-3.484 3.563-3.484h4.367c.563 0 1.02-.458 1.02-1.02V1.03c0-.562-.456-1.018-1.02-1.02z"/></svg>'
            }]
          }, {
            block: 'link',
            mix: [{
              block: 'b-soc-icons',
              elem: 'icon',
            }],
            url: 'http://vk.com/foxexpo',
            content: [{
              block: 'icon',
              content: '<svg fill="#ec8660" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 49.999997 50"><path d="M47.93 33.772c-1.49-1.785-3.242-3.31-4.9-4.927-1.495-1.46-1.59-2.305-.387-3.99 1.312-1.836 2.723-3.603 4.047-5.432 1.236-1.71 2.5-3.414 3.15-5.46.412-1.303.047-1.88-1.288-2.107-.23-.04-.468-.042-.702-.042l-7.923-.01c-.975-.013-1.515.412-1.86 1.277-.466 1.17-.946 2.338-1.5 3.466-1.26 2.558-2.667 5.03-4.638 7.123-.436.46-.917 1.044-1.643.804-.91-.332-1.177-1.83-1.162-2.336l-.008-9.144c-.176-1.306-.466-1.888-1.763-2.142h-8.23c-1.098 0-1.648.426-2.235 1.113-.34.397-.44.654.254.786 1.366.26 2.135 1.144 2.34 2.513.327 2.19.304 4.386.116 6.583-.056.64-.166 1.28-.42 1.88-.4.942-1.04 1.133-1.885.554-.765-.524-1.3-1.264-1.827-2.007-1.973-2.79-3.548-5.797-4.832-8.957-.37-.913-1.01-1.466-1.98-1.48-2.375-.04-4.753-.045-7.13 0-1.43.028-1.856.722-1.274 2.02 2.59 5.757 5.47 11.355 9.232 16.455 1.93 2.618 4.148 4.93 7.013 6.566 3.246 1.856 6.74 2.415 10.42 2.243 1.725-.082 2.242-.53 2.322-2.248.053-1.175.186-2.344.766-3.4.57-1.036 1.43-1.233 2.424-.59.497.324.916.735 1.31 1.168.97 1.06 1.906 2.155 2.913 3.18 1.263 1.285 2.76 2.044 4.64 1.89h7.28c1.174-.076 1.782-1.514 1.11-2.824-.474-.917-1.095-1.733-1.754-2.524z"/></svg>'
            }]
          }, {
            block: 'link',
            url: 'https://plus.google.com/u/0/105325706331366851335',
            content: [{
              block: 'icon',
              content: '<svg fill="#ec8660" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 49.999997 50"><path d="M27.293 3.878h-13.08c-5.863 0-11.382 4.444-11.382 9.59 0 5.26 3.998 9.507 9.966 9.507l1.213-.037c-.388.74-.666 1.574-.666 2.444 0 1.46.787 2.646 1.78 3.612l-2.268.025C5.59 29.015 0 33.646 0 38.44c0 4.73 6.132 7.682 13.394 7.682 8.28 0 12.856-4.7 12.856-9.425 0-3.788-1.118-6.06-4.575-8.504-1.18-.837-3.444-2.87-3.444-4.068 0-1.403.4-2.094 2.514-3.744 2.162-1.69 3.694-4.068 3.694-6.83 0-3.29-1.465-6.5-4.22-7.556h4.148l2.928-2.116zm-4.565 32.006c.103.438.16.885.16 1.35 0 3.822-2.46 6.803-9.52 6.803-5.02 0-8.646-3.178-8.646-6.997 0-3.74 4.496-6.853 9.518-6.803 1.172.016 2.266.2 3.257.523 2.724 1.89 4.68 2.962 5.23 5.125zm-8.04-14.243c-3.372-.1-6.575-3.77-7.157-8.196-.58-4.428 1.68-7.816 5.048-7.716 3.37.1 6.575 3.653 7.156 8.078.585 4.428-1.678 7.935-5.046 7.835zM40.625 3.88H37.5v9.272h-9.375v3.125H37.5v9.478h3.125v-9.478H50V13.15h-9.375"/></svg>'
            }]
          }]
        }, {
          elem: 'i',
          elemMods: { row: true },
          content: [{
            block: 'b-copyrights',
            content: '© foxexpo.by 2015'
          }, {
            block: 'b-footer',
            elem: 'cms-link',
            content: 'Работает на <a href="http://www.hostcms.ru" title="Система управления сайтом HostCMS">HostCMS</a>'
          }]
        }]
      }]
    }, {
      elem: 'i-group',
      content: [{
        elem: 'i-group',
        elemMods: { 'icon': true },
        content: {
          block: 'image',
          content: '<svg fill="#ec8660" height="30" viewBox="0 0 24 24" width="30" xmlns="http://www.w3.org/2000/svg"><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/><path d="M0 0h24v24H0z" fill="none"/><path d="M12.5 7H11v6l5.25 3.15.75-1.23-4.5-2.67z"/></svg>'
        }
      }, {
        elem: 'i',
        content: [{
          elem: 'i',
          content: ['<p>Время работы: Пн-Пт &ndash; c 09:00 до 18:00</p>', '<p>Выходной: Сб-Вс</p>']
        }]
      }]
    }]
  }]
}
