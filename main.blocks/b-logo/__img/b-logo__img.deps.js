({
  mustDeps: [],
  shouldDeps: [{
    elems: [{
      elem: 'img',
      mods: {
        hidden: 'xs'
      }
    }, {
      elem: 'img',
      mods: {
        visible: 'xs'
      }
    }, {
      elem: 'img',
      mods: {
        responsive: true
      }
    }, {
      elem: 'img',
      mods: {
        state: 'scroll'
      }
    }]
  }]
})
