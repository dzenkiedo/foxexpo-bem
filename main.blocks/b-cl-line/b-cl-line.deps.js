({
  mustDeps: [],
  shouldDeps: ['b-vars']
}, {
  shouldDeps: [{
    block: 'b-cl-line',
    elems: [{
      elem: 'caption'
    }, {
      elem: 'items'
    }, {
      elem: 'item'
    }, {
      elem: 'img'
    }]
  }]
})
