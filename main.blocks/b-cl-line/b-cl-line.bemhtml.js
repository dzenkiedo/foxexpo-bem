block('b-cl-line')(
  content()(function(){
    return [
      {
        elem: 'caption',
        content: this.ctx.caption
      },
      {
        elem: 'items',
        tag: 'ul',
        mix : [ { block : 'clearfix' } ],
        content: (function(clients){
          return (clients || [clients]).map(function(client) {
            return {
              elem : 'item',
              tag: 'li',
              content : [
                {
                  block : 'image',
                  mix: { block: 'b-cl-line', elem: 'img' },
                  url: client,
                  alt: 'client Item',
                  height: 'auto',
                  width: 'auto'
                }
              ]
            }
          });
        })(this.ctx.clients)
      }
    ];
  }),
  elem('item')(
    content()(function(){
      return applyNext();
    })
  )
);