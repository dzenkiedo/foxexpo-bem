modules.define('b-slide', ['i-bem__dom', 'functions__throttle', 'jquery'], function(provide, BEMDOM, throttle, $) {


  function winWidthIsLt(breakpoint) {

    var bp = (breakpoint !== null || undefined) ? breakpoint : 840;
    return (window.innerWidth < bp) ? true : false;
  }

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      'js': {
        'inited': function() {}
      },
      'active': {
        '*': function() {
          this.setMod(this.elem('i-img-thumb'), 'slide-switcher', 'active');
        },
/*        'left': function() {
          this.setMod(this.elem('i-img-thumb'), 'slide-switcher', 'active');
        },
        'right': function() {
          this.setMod(this.elem('i-img-thumb'), 'slide-switcher', 'active');
        },
*/        '': function() {
          this.delMod(this.elem('i-img-thumb'), 'slide-switcher', 'active');
          this.setMod(this.elem('i-img-thumb'), 'slide-switcher', true);
        }
      }

    }

  }));
});
