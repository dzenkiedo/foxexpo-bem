({
  mustDeps: ['b-vars'],
  shouldDeps: [{
    elems: ['body', 'content', 'footer', { elem: 'i-img-thumb', elemMods: { 'slide-switcher': [true, 'active'] } }, 'img', { elem: 'img-group', mods: { hidden: 'xs' } }, 'p-item', 'title'],
    mods: { active: [true,'left','right'], prev: ['left','right'] }
  }]
})
