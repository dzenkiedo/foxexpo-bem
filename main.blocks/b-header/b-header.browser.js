modules.define('b-header', ['i-bem__dom', 'functions__throttle', 'jquery'], function(provide, BEMDOM,throttle,$) {

  function winWidthIsLt(breakpoint) {
    // console.log(typeof breakpoint);
    var bp = (breakpoint !== null || undefined) ? breakpoint: 840;
      return (window.innerWidth < bp) ? true : false;
  };

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      js: {
        inited: function() {
          // debugger;
          this._onResize();
          this.bindToWin('scroll', throttle(this._onScroll, 300));
          this.bindToWin('resize', throttle(this._onResize, 300));
        }
      }
    },
    _onScroll: function(e) {
      // debugger;
      var header = this;
      var logo = this.findBlockInside('b-logo');
      if ($(window).scrollTop() > header.domElem.height()) {
        this.setMod('state','scroll');
        logo.setMod(logo.elem('img'), 'state','scroll');
      } else {
        if(this.hasMod('state','scroll')) {
          // console.log(this);
          this.delMod('state');
          logo.delMod(logo.elem('img','state','scroll'), 'state')
          
        }
      }
    },
    _onResize: function(e) {
      if(winWidthIsLt(840)) {
        this.delMod('fixed');
      } else {
        this.setMod('fixed');
      }
    }
  }));
})