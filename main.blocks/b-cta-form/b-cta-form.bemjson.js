{
  elem: 'i-right',
  content: [{
    block: 'b-cta-form',
    tag: 'form',
    mods: {
      vert: true
    },
    content: [{
      elem: 'caption',
      content: [
        'Заказать сейчас'
      ]
    }, {
      elem: 'input-group',
      mix: [{
        block: 'control-group'
      }],
      elemMods: {
        left: true
      },
      content: [{
        block: 'input',
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            text: true
          }
        }],
        mods: {
          focused: false
        },
        placeholder: 'Ваше имя'
      }, {
        block: 'input',
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            text: true
          }
        }],
        mods: {
          focused: false
        },
        placeholder: 'Телефон'
      }, {
        block: 'input',
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            text: true
          }
        }],
        mods: {
          focused: false
        },
        placeholder: 'e-mail'
      }]
    }, {
      elem: 'input-group',
      mix: [{
        block: 'control-group'
      }],
      elemMods: {
        right: true
      },
      content: {
        block: 'textarea',
        mods: {
          width: 'available'
        },
        mix: [{
          block: 'b-cta-form',
          elem: 'input',
          elemMods: {
            textarea: true
          }
        }],
        placeholder: 'Комментарий'
      }
    }, {
      elem: 'input-group',
      mix: [{
        block: 'control-group'
      }],
      content: {
        block: 'button',
        mix: [{
          block: 'b-cta-form',
          elem: 'button',
          elemMods: {
            submit: true
          }
        }],
        mods: {
          focused: false
        },
        text: 'Заказать'
      }
    }]
  }]
}]
}
