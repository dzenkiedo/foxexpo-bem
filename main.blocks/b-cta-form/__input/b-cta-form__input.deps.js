({
  mustDeps: [],
  shouldDeps: [{
    block: 'b-vars',
  },{
    block: 'input',
    mods: { valid: true, valid:'email', 'has-required': true },
    elems: [{ elem: 'required', mods:'valid'}]
  }]
})