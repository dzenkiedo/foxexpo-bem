modules.define('b-cta-form', ['i-bem__dom', 'jquery'], function(provide, BEMDOM, $) {

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      js: {
        inited: function(e) {

          this._bctaformPopup = this.findBlockInside({ block: 'popup' });
          this._bctaformPopupLink = this.findBlockInside({ block: 'link' });
          this._bctaformPopup.setAnchor(this._bctaformPopupLink);
          this._valid = {
            email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          }
        }
      }
    },
    _onSubmit: function(e) {

      var bctaformFields = this.findBlocksInside({ block: 'input', modName: 'has-required', modVal: true });
      var bctaformFieldsChecked = bctaformFields.every(function(bctaformField) {
        return this._fieldIsValid(bctaformField);
        // debugger;
      }, this);
      // debugger;
      e.preventDefault();
      if (bctaformFieldsChecked) {
        var bctaformData = new FormData(this.domElem[0]);
        // debugger;
        $.ajax({
          url: this.domElem.attr('action'),
          data: bctaformData,
          type: 'post',
          processData: false,
          contentType: false,
          context: this,
          beforeSend: function(xhr) {
            $('button').bem({ block: 'button' }).findBlockInside({ block: 'spin' }).toggleMod('visible');
          }
        }).complete(function(data) {
          // debugger;
          console.log(data);
          console.log(data.success === true);
          debugger;
          $('button').bem({ block: 'button' }).findBlockInside({ block: 'spin' }).toggleMod('visible');
          if (data.responseJSON.success) {
            this._bctaformPopup.setMod('visible');
            this._bctaformPopup.setContent(data.responseJSON.responseMessage);
          }
        });
      } else {
        this._bctaformPopup.setMod('visible');
        this._bctaformPopup.setContent(this._bctaformPopup.params.popupDefaultErrMsg);
      }
      return false;
      // this.domElem.submit();
    },
    _onInputChange: function(input, ev) {
      // debugger;
      input.setMod(input.elem('required'), 'valid', this._fieldIsValid(input));
    },
    _fieldIsValid: function(field) {
      var val = field.elem('control').val();
      var type = field.getMods().valid;
      if (!this._valid.hasOwnProperty(type)) {
        return val !== '';
      } else {
        return this._valid[type].test(val);
      };
    }
  }, {
    live: function(e) {
      this.liveBindTo('button', 'pointerclick', function(e) {
        this._onSubmit(e);
      });
      this.liveBindTo('input', 'change pointerout', function(e) {
        // debugger;
        var inputIsRequired = this.findBlockOn(e.currentTarget, { block: 'input', mods: { 'has-required': true } })
        if (inputIsRequired) {
          this._onInputChange(inputIsRequired, e);
        }
      })
      return false;
    }
  }));

})
