block('b-promo-gallery')(
  content()(function(){
    return {
      elem: 'items',
      content: (function(promoslides){
        return (promoslides || [promoslides]).map(function(promoslide) {
          return {
            elem : 'item',
            content : [
              {
                block : 'image',
                mix: { block: 'b-promo-gallery', elem: 'img' },
                url: promoslide.img,
                alt: 'Gallery Item',
                height: 'auto',
                width: 'auto'
              },
              {
                elem: 'desc-group',
                content: [
                  {
                    elem: 'item',
                    tag: 'span',
                    elemMods : { desc : true },
                    content: promoslide.desc
                  },
                  {
                    elem: 'item',
                    tag: 'span',
                    elemMods : { price : true },
                    content: promoslide.price
                  }
                ]
              }
            ]
          }
        });
      })(this.ctx.promoslides)
    };
  }),
  elem('item')(
    content()(function(){
      return applyNext();
    })
  )
);