module.exports = function(bh) {
  bh.enableInfiniteLoopDetection(true);
  bh.match('b-promo-gallery', function(ctx, json) {
    ctx.content({
      elem: 'items',
      content: (json.promoslides || [json.promoslides]).map(function(promoslide) {
        return {
          elem: 'item',
          content: [{
            block: 'link',
            tag: 'a',
            url: '#',
            title: 'promo_item',
            content: [{
              block: 'image',
              mix: { block: 'b-promo-gallery', elem: 'img' },
              url: promoslide.img,
              alt: 'Gallery Item',
              height: '100%',
              width: '100%'
            }, {
              block: 'b-promo-gallery',
              elem: 'desc-group',
              content: [{
                elem: 'item',
                tag: 'span',
                elemMods: { desc: true },
                content: promoslide.desc
              }, {
                block: 'b-promo-gallery',
                elem: 'item',
                tag: 'span',
                elemMods: { price: true },
                content: promoslide.price
              }]
            }]
          }]
        }
      })
    });
  });
}
