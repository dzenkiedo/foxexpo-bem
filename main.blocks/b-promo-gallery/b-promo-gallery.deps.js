({
  techs: 'stylus',
  mustDeps: [{
    block: 'b-vars'
  }],
  shouldDeps: []
},
{
  shouldDeps: [{
    elems: [{
      elem: 'items'
    },
    {
      elem: 'item'
    },{
      elem: 'img'
    },
    {
      elem: 'desc-group'
    },
    {
      elem: 'item',
      mods: { desc: true }
    },{
      elem: 'item',
      mods: { price: true }
    }]
  }]
})
