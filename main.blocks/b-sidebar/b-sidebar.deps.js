({
  mustDeps: [{
    block: 'i-bem',
    elem: 'dom',
  },
  'b-vars', 'b-footer'],
  shouldDeps: [{
      mods: { 'position': ['fixed', 'fixed-bottom'], 'l-head': true }
    },
    { block: 'functions', elem: 'throttle' },
  ]
})
