modules.define('b-sidebar', ['i-bem__dom', 'functions__throttle', 'jquery', 'b-header', 'b-footer'], function(provide, BEMDOM, throttle, $, header, footer) {
  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      'js': {
        'inited': function(e) {
          this._apos = this.domElem[0].getBoundingClientRect().bottom;
          this.bindToWin('scroll', function(e) {
            throttle(this._onScroll(e,header,footer), 300);
          })
        }
      }
    },
    _onScroll: function(e,header,footer) {
      // debugger;
      var sidebar = this;

      if (!this.hasMod('l-head') && $(window).scrollTop() > $(header.buildSelector()).height()) {
        // debugger;
        if($(document).outerHeight() - $(document).scrollTop() > $(window).outerHeight()
          && $(document).outerHeight() - $(document).scrollTop() - this._apos > $(footer.buildSelector()).height()) {
          sidebar.setMod('position', 'fixed');
          this._apos = this.domElem[0].getBoundingClientRect().bottom;
        } else {
          if ( $(document).outerHeight() - $(document).scrollTop() - this._apos < $(footer.buildSelector()).height() )
          {
          sidebar.setMod('position', 'fixed-bottom');
          }
        }

      } else {
        if (this.hasMod('position', 'fixed') || this.hasMod('position', 'fixed-bottom')) {
          // console.log(this);
          this.delMod('position');
        }
      }
    }
  }));
})