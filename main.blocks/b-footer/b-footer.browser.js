modules.define('b-footer', ['i-bem__dom', 'functions__throttle', 'jquery'], function(provide, BEMDOM,throttle,$) {

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      js: {
        inited: function() {
          console.log('b-footer init done');
        }
      }
    }
  }));
})