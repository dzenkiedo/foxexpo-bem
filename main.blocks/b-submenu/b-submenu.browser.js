modules.define('b-submenu', ['i-bem__dom', 'functions__throttle', 'jquery'], function(provide, BEMDOM, throttle, $) {

  function winWidthIsLt(breakpoint) {
    // console.log(typeof breakpoint);
    var bp = (breakpoint !== null || undefined) ? breakpoint : 840;
    return (window.innerWidth < bp) ? true : false;
  };

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      'js': {
        'inited': function() {
          /*          this.bindTo('pointerenter', function(e) {
                      this.(e,this);
                    });*/
          // debugger;
          this._onResize();
          this.bindToWin('resize', throttle(this._onResize, 500));
          this.bindTo('controls', 'pointerclick', this._onClick);
          this.bindTo('pointerenter pointerleave', this._onHover);
        }
      },
      '': function() {
        return false;
      }
    },

    _onHover: function(e, ctx) {
      // debugger;
      // console.log(e.type);
      // console.log(e.relatedTarget);
      // console.log(e.type);
      if (this.hasMod('autohover') && this.getMod('type') !== 'vert') {

        // debugger;
        this._toggleContentState(e);
      }
    },
    _onResize: function(e) {
      if (this.getMod('type') !== 'vert') {
        if (winWidthIsLt(840)) {
          this.hasMod('autohover') && this.delMod('autohover');
        } else {
          this.setMod('autohover');
          this._close();
        }
      }
    },
    _close: function(e) {
      this.findBlockOn(this.elem('controls'), 'button').delMod('checked');
      this.setMod(this.elem('content'), 'status', 'closed');
    },
    _toggleContentState: function(e) {
      var contentStatus = this._getElemMod('status', this.elem('content'));
      this.toggleMod(this.elem('content'), 'status', contentStatus, contentStatus === 'closed' ? 'open' : 'closed');
    },
    _toggleState: function(e) {
      // debugger;
      var buttonStatus = this.findBlockOn(this.elem('controls'), 'button').getMod('checked');
      var contentStatus = this._getElemMod('status', this.elem('content'));
      // debugger;
      // this.findBlockOn(this.elem('controls'),'button').toggleMod('checked');
      this.toggleMod(this.elem('content'), 'status', contentStatus, contentStatus === 'closed' ? 'open' : 'closed');
    },
    _onClick: function(e) {
      // debugger;
      e.preventDefault();
      if (this.hasMod('autohover') && this.getMod('type') !== 'vert') {
        this.findBlockInside('button').setMod('checked', false);
        this.findBlockInside('button').delMod('focused', false);
        location.href = this.findBlockInside('button').domElem.attr('href');
        return false;
      } else {
        if (!this.hasMod('autohover')) {
          this.emit('submenuClick');
          this._toggleState(e);
        }
        // debugger;
      }

      // debugger;
    }

  }));

})
