({
  mustDeps: ['b-vars'],
  shouldDeps: [{
    block: 'b-submenu',
    elems: [{
      elem: 'content',
      mods: {
        status: ['open','closed']
      }
    }]
  }]
})