({
  techs: 'stylus',
  mustDeps: [{
    block: 'b-vars'
  }],
  shouldDeps: [{
    block: 'b-submenu',
    elems: [{
      elem: 'icon',
      mods: { dropdown: 'open' }
    },{
      elem: 'icon',
      mods: { dropdown: 'closed' }
    }, {
      elem:'icon',
      mods: { bg: 'white' }
    }, {
      elem: 'icon',
      mods: { bg: 'orange' }
    }]
  }]
})