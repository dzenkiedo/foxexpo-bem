({
  mustDeps: [
    'b-menu',
    'events',
    'b-vars',
    'jquery', {
      block: 'i-bem',
      elem: 'dom',
    },
    { block: 'functions', elem: 'throttle' }
  ],
  shouldDeps: [{
    mods: { type: 'vert' }
  }]
})
