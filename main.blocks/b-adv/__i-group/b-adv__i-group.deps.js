({
  mustDeps: [],
  shouldDeps: [
    'b-vars',
    { mods: ['left', 'right', 'center'] }
  ]
})
