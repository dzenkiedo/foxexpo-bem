block('input').mod('has-required', true).elem('box')
    .content()(function() {
        return this.extend([{ elem : 'required' , content: '*'}, applyNext()]);
    });