[{
    mustDeps : { block : 'i-bem', elems : 'dom' },
    shouldDeps : [
        {
            mods : { hidden: true, disabled : true, focused : true },
            elems : ['box', 'control']
        },
        'control'
    ]
}]
