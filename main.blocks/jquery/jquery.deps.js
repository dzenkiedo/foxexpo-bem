({
  shouldDeps: [
    { block: 'loader', mods: { type: 'js' } },
    { elem: 'config' },
    { elem: 'event', mods: {modName: 'type', modVal: 'pointer'}}
  ]
})