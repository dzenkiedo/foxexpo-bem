({
  mustDeps: [{
    block: 'b-vars'
  }],
  shouldDeps: []
}, {
  mustDeps: [{
    // block: 'b-logo',
    // elems: [
    //   {
    //     elem: 'img',
    //     mod: { hidden:'xs' }
    //   }
    // ],
    // tech: 'stylus'
  }],
  shouldDeps: [{
    elems: [{
      elem: 'img'
    }, {
      elem: 'img',
      mods: [{
        responsive: true
      }]
    }]
  }]
})
