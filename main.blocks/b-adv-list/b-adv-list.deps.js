({
  mustDeps: [],
  shouldDeps: ['b-vars']
}, {
  shouldDeps: [{
    block: 'b-adv-list',
    elems: [{
      elem: 'i',
      mods: {
        'c-wrap': true
      }
    }, {
      elem: 'i',
      mods: {
        caption: true
      }
    }, {
      elem: 'i',
      mods: {
        desc: true
      }
    }, {
      elem: 'i',
      mods: {
        icon: true
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'bbook'
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'cook'
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'p-design'
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'printer'
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'stand'
      }
    }, {
      elem: 'i',
      mods: {
        icon: 'web-design'
      }
    },{
      elem: 'i',
      mods: {
        icon: 'svg'
      }
    },{
      elem: 'i',
      mods: {
        icon: 'img'
      }
    }
    ]
  }]
})
