module.exports = function(bh) {
  bh.enableInfiniteLoopDetection(true);
  bh.match('b-adv-list', function(ctx, json) {
    ctx.content((json.advlist || [json.advlist]).map(function(advItem) {
      return {
        elem: 'i',
        tag: 'li',
        content: [{
          elem: 'i',
          elemMods: { 'c-wrap': true },
          content: [{
            elem: 'i',
            elemMods: { icon: true },
            mix: [{
              elem: 'i',
              elemMods: {
                icon: (function(advItem) {
                  if (advItem.icon.src !== undefined) {
                    return 'img';
                  } else {
                    return advItem.icon
                  }
                })(advItem)
              }
            }],
            content: (function(advItem) {
              if (advItem.icon.src) {
                return [{
                  block: 'image',
                  url: advItem.icon.src
                }];
              } else {
                return [''];
              }
            })(advItem)
          }, {
            elem: 'i',
            elemMods: { caption: true },
            content: [advItem.caption]
          }]
        }, {
          elem: 'i',
          elemMods: { desc: true },
          content: [advItem.desc]
        }]
      };
    }));
  });
}
