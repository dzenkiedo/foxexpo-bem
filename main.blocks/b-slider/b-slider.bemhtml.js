block('b-slider')(
  js()(true),
  content()(function() {
    var slidethumbs = [];
    var slides = [];
    this.ctx.slidelist.forEach(function(slide, index) {
      var slideId = 'slide' + index;

      slidethumbs.push({
        block: 'b-slide',
        tag: 'i',
        mix: [{
          block: 'b-slider',
          elem: 'i-img-thumb'
        }, {
          elem: 'i-img-thumb',
          elemMods: {
            'slide-switcher': (index === 0 || index == 2) ? 'active' : true
          }
        }],
        js: {
          id: slideId
        },
        mods: { 'active': (index === 0 || index === 2) ? true : false },
      });

      slides.push({
        block: 'b-slide',
        tag: 'li',
        js: {
          id: slideId
        },
        mix: [{
          block: 'b-slider',
          elem: 'slide',
          elemMods: { active: (index == 0) ? true : false }
        }],
        mods: { active: (index === 0 || index === 2) ? true : false },
        content: [{
          elem: 'img-group',
          elemMods: {
            hidden: 'xs'
          },
          content: {
            block: 'link',
            url: '#' + index,
            content: {
              block: 'b-slide',
              elem: 'img',
              tag: 'img',
              attrs: {
                src: slide.image.url,
                alt: slide.image.alt
              }
            }
          }
        }, {
          elem: 'content',
          content: [/*{
            elem: 'title',
            content: slide.title
          }, {
            elem: 'body',
            content: slide.properties.map(function(property) {
              var modds = {};
              modds[property.icon] = true;
              return [{
                elem: 'p-item',
                content: [{
                  block: 'b-icon',
                  tag: 'span',
                  mods: modds
                }, {
                  elem: 'p-item',
                  tag: 'span',
                  elemMods: {
                    desc: true
                  },
                  content: property.desc
                }]
              }]
            })
          }, */{
            elem: 'footer',
            content: [/*{
              elem: 'footer',
              elemMods: {
                desc: true
              },
              content: slide.footer
            }, */{
              block: 'button',
              mix: [{ block: 'b-slide', elem: 'footer', elemMods: { 'button': true } }],
              mods: { theme: 'islands', size: 'xl', type: 'link' },
              url: '#',
              text: 'Подробнее'
            }]
          }]
        }]
      });
    });


    return [{
      elem: 'i-img-thumbs',
      mods: { visible: 'xs' },
      content: slidethumbs
    }, {
      elem: 'i',
      tag: 'ul',
      content: slides
    }, {
      block: 'b-slider',
      elem: 'i-control',
      elemMods: {
        left: true
      },
      content: {
        block: 'image',
        content: '<svg xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24.0 44.5"width="24.0"height="44.5"><path class="b-slider__i-control_arrow" d="M21.76 42.27L1.25 21.76 21.76 1.25v12.388l-8.122 8.12 8.12 8.122v12.39z" fill="none" stroke="#afafaf" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'
      }
    }, {
      block: 'b-slider',
      elem: 'i-control',
      elemMods: {
        right: true
      },
      content: {
        block: 'image',
        content: '<svg xmlns="http://www.w3.org/2000/svg"viewBox="0 0 24.0 44.5"width="24.0"height="44.5"><path class="b-slider__i-control_arrow" d="M21.76 42.27L1.25 21.76 21.76 1.25v12.388l-8.122 8.12 8.12 8.122v12.39z" fill="none" stroke="#afafaf" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'
      }
    }]
  }));
