({
  mustDeps: [
    { block: 'i-bem', elems: 'dom' },
    { block: 'functions', elem: 'throttle' }
  ],
  shouldDeps: [
    'b-slide',
    'b-icon',
    'b-vars', {
      elems: ['i', 'i-control', 'i-img-thumbs', 'slide']
    }
  ]
})
