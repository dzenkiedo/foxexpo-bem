modules.define('b-slider', ['i-bem__dom', 'functions__throttle', 'jquery'], function(provide, BEMDOM, throttle, $) {


  function winWidthIsLt(breakpoint) {

    var bp = (breakpoint !== null || undefined) ? breakpoint : 840;
    return (window.innerWidth < bp) ? true : false;
  }

  provide(BEMDOM.decl(this.name, {
    beforeSetMod: {
      'js': {
        'inited': function() {
          var slides = this.findBlocksInside('b-slide');
          var firstItem = slides.shift();
          firstItem.setMod('active', true);
          slides.forEach(function(slide) {
            slide.delMod('active');
          })
        }
      }
    },

    onSetMod: {
      'js': {
        'inited': function() {
          // debugger;
          this._ratio = 0.0263;
          // this._ratio = 0.03542;
          this._onResize();
          // if (window.innerWidth > 768) {
          //   var height = this.domElem.width() * this._ratio;
          //   this.elem('i').height(height + 'rem');
          // } else {
          //   this.elem('i').height('auto');
          // };
          this.bindTo(this.elem('i-control', 'left', true), 'pointerclick', this._prevSlide);
          this.bindTo(this.elem('i-control', 'right', true), 'pointerclick', this._nextSlide);
          this.bindTo('i-img-thumb', 'pointerclick', function(e) {
            // debugger;
            // console.log($);
            // var slideId = e.currentTarget();
            // debugger;
            var slideId = this.findBlockOn($(e.currentTarget), 'b-slide');
            // debugger;
            this._setActive(slideId);
          });
          this.bindTo('pointerenter', this._slideShowStop);
          this.bindTo('pointerleave', this._slideShowStart);
          this.bindToWin('resize', throttle(this._onResize, 300));
          var slides = this.findBlocksInside('b-slide');
          if (this.findBlockInside({ block: 'b-slide', modName: 'active', modVal: true }) !== null) {
            this._currentSlide = this.findBlockInside({ block: 'b-slide', modName: 'active', modVal: true });
          } else {
            this.findBlockInside({ block: 'b-slide' }).setMod('active');
          }
          // var slideThumbs = this.elem('i-img-thumb');
          this.params.slideList = slides.map(function(slide, index) {
            // debugger;
            return slide;
          });
          // debugger;
          this._slideShowStart();
        }
      }
    },
    onElemSetMod: {
      'slide': {
        'active': {
          true: function() {
            this._currentSlide = this.findBlocksInside('b-slide_active')[0];
          }
        }
      }
    },
    _onResize: function(ratio) {
      if (winWidthIsLt(600)) {
        this.domElem.height('auto');
        this.elem('i').height('auto');
      } else {
        var width = this.domElem.width();
        this.domElem.height(Math.round(width * this._ratio * 10)/10 + 'rem');
        this.elem('i').height(Math.round(width * this._ratio * 10)/10 + 'rem');
      }
    },
    _onClick: function(e) {
      // debugger;
      if (this.getMods(e.currentTarget).hasOwnProperty('left')) {
        this._prevSlide(e);
      } else {
        this._nextSlide(e);
      }
    },
    _prevSlide: function(e) {
      var prevSlide;
      // debugger;
      if (this._getActive().domElem.prev().length == 0) {
        prevSlide = this.findBlockOn(this._getActive().domElem.eq(1).siblings().last(), 'b-slide');
      } else {
        prevSlide = this.findBlockOn(this._getActive().domElem.eq(1).prev(), 'b-slide');
      }
      this._getActive().delMod('active').setMod('prev', 'right');
      prevSlide.delMod('prev').setMod('active', 'right');
      // debugger;
    },
    _nextSlide: function(e) {
      // debugger;
      var nextSlide;
      if (this._getActive().domElem.eq(1).next().length == 0) {
        nextSlide = this.findBlockOn(this._getActive().domElem.eq(1).siblings().first(), 'b-slide');
        // nextSlide = this.findBlocksInside('b-slide');
      } else {
        // debugger;
        nextSlide = this.findBlockOn(this._getActive().domElem.eq(1).next(), 'b-slide');
      }
      // debugger;
      this._getActive().delMod('active').setMod('prev', 'left');
      nextSlide.delMod('prev').setMod('active', 'left');
      // debugger;
    },
    _slideShowStart: function() {
      var cttx = this;
      if (this._slideShowInterval) {
        this._slideShowStop();
      }
      this._slideShowInterval = setInterval(
        function() {
          cttx._nextSlide(cttx);
        }, 5000
      );
    },
    _slideShowStop: function() {
      var cttx = this;
      clearInterval(this._slideShowInterval);
    },
    _getActive: function() {
      var active = false;
      this.findBlocksInside('b-slide').forEach(function(slide) {
          if (slide.hasMod('active')) {
            active = slide;
          };
        })
        // debugger;
      return active;
    },
    _setActive: function(slide) {
      // debugger;
      this._getActive().delMod('active');
      slide.delMod('prev').setMod('active', true);
    }

  }));
});
