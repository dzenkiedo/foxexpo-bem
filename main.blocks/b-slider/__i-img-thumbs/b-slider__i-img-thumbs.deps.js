({
  mustDeps: ['b-vars'],
  shouldDeps: [{
    block: 'b-slider',
    elem: 'i-img-thumbs',
    mods: { visible:'xs'}
  }]
})
