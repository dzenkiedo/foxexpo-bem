({
  techs: 'stylus',
  mustDeps: [],
  shouldDeps: [{
    block: 'b-menu',
    elems: [{
      elem: 'icon',
      mods: {
        background: 'orange'
      }
    }, {
      elem: 'icon',
      mods: {
        content: 'open'
      }
    }, {
      elem: 'icon',
      mods: {
        content: 'closed'
      }
    }]
  }, {
    block: 'b-vars'
  }]
})
