modules.define('b-menu', ['i-bem__dom', 'functions__throttle', 'jquery', 'b-submenu'], function(provide, BEMDOM, throttle, $, bSubmenu) {
  function winWidthIsLt(breakpoint) {
    // console.log(typeof breakpoint);
    var bp = (breakpoint !== null || undefined) ? breakpoint : 840;
    return (window.innerWidth < bp) ? true : false;
  };

  provide(BEMDOM.decl(this.name, {
    onSetMod: {
      'js': {
        'inited': function() {
          // debugger;
          this._onResize();
          this.bindToWin('resize', throttle(this._onResize, 500));
          this.bindTo(this.elem('controls'), 'pointerclick', this._onClick);
          bSubmenu.on(this.domElem, 'submenuClick', this._onItemWithSubmenuClick, this);
        }
      },
      '': function() {
        return false;
      },
    },
    _onResize: function(e) {

      var controlsExists = this.findElem('controls').length;
      controlsExists && this.findBlockInside(this.elem('controls'), 'button').delMod('checked');
      if (winWidthIsLt(840)) {
        controlsExists && this.setMod(this.elem('controls'), 'visible', 'xs');
        this.setMod(this.elem('content'), 'status', 'closed');
      } else {
        controlsExists && this.delMod(this.elem('controls'), 'visible');
        this.setMod(this.elem('content'), 'status', 'open');
      }
    },
    _onClick: function(e) {

      var submenuItems = this.findBlocksInside({ block: 'b-submenu' });

      if (submenuItems.length > 0 && this.getMod(this.elem('content'), 'status', 'open')) {
        submenuItems.forEach(function(submenuItem) {
          submenuItem._close();
        });
      };
      this.toggleMod(this.elem('content'), 'status', 'open', 'closed');

    },
    _onItemWithSubmenuClick: function(e) {
      // debugger;
      this.findBlocksInside('b-submenu').forEach(function(submenuItem) {
        if (submenuItem._uniqId !== e.target._uniqId) {
          submenuItem._close();
        }
      });
    }
  }));
})
