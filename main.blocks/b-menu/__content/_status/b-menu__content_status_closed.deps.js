({
  mustDeps: [{
    block: 'b-vars'
  }],
  shouldDeps: [{
    block: 'b-menu',
    elem: 'content',
    mods: { status: 'open' }
  }]
})
