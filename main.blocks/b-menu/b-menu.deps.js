({
  mustDeps: [
    'events',
    'b-vars',
    'jquery', {
      block: 'i-bem',
      elem: 'dom',
    },
    { block: 'functions', elem: 'throttle' }
  ],
  shouldDeps: [{
      elems: [{
        elem: 'controls',
        mods: { 'visible': 'xs' }
      }]
    },
  ]
})
